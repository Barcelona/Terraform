# litecoind

Provisions an host to run a simple container for litecoind in nomad.

Changes should be followed by a  `terraform apply`.

## Notes

The nomad job specification limits the container to run on logical cores 0 and 1, but that does not prevent the OS from scheduling other processes to run on these cores. If exclusive core access is required, then some considerations need to be made, first off, core 0 can never be fully isolated due to the kernel running on core 0, so the value would have to be changed initially to something like 'cores = 1-2'. Afterwards, you will need to add an `isolcpu` statement to your kernel command line, such as:
`isolcpu=1,2`. This will prevent processes from automatically scheduling on the cores that are being specified in this nomad, and ensuring exclusive access to those cores.
