job "litecoin" {
  datacenters = ["dc1"]
  type        = "service"

  group "litecoin-group" {
    count = 1

    network {
      port "litecoin" {static = 9333}
      port "litecoin-rpc" {static = 9332}
    }

    task "litecoin-task" {
      driver = "docker"

      config {
        image = "somerepo/litecoin:0.21.2.2"
        ports = ["litecoin", "litecoin-rpc"]
      }

      service {
        name = "litecoin-service"
        port = "litecoin"

        check {
          type     = "tcp"
          port     = "litecoin"
          interval = "30s"
          timeout  = "10s"
        }
      }

      resources {
        memory = 1024 # 1024MB
        cores  = 0-1 # 2 logical cores
      }

      env {
        # Normally you would fetch these from Vault, but for validation, you can hard-code or omit
        LITECOIN_RPC_USER     = "your_rpc_user"
        LITECOIN_RPC_PASSWORD = "your_rpc_password"
      }
    }
  }
}
