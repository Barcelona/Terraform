resource "nomad_job" "litecoin_job" {
  # Use the file() function to read the Nomad job specification from a file
  jobspec = file("${path.module}/litecoind.nomad")
}
